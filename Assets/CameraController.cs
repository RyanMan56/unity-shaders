﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {
    public RawImage raw;
    public float intensity;
    public RectTransform rectTransform;

    WebCamTexture camTexture;
    Vector2 resolution;
    float ratio;
    Material mat;

    // Use this for initialization
    void Start () {
        camTexture = new WebCamTexture();

        SetProjection();        

        raw.texture = camTexture;
        raw.material.mainTexture = camTexture;
        
        camTexture.Play();
	}

    private void Awake()
    {
        mat = new Material(Shader.Find("Hidden/BWDiffuse"));
    }

    // Update is called once per frame
    void Update () {
        if (resolution != new Vector2(camTexture.width, camTexture.height) || resolution != new Vector2(camTexture.width, camTexture.height))
        {
            SetProjection();
        }        
	}

    void SetProjection()
    {
        resolution = new Vector2(camTexture.width, camTexture.height);
        ratio = camTexture.width / camTexture.height;
        rectTransform.sizeDelta = resolution;
        float scaleMultiplier = Screen.width / rectTransform.sizeDelta.x;
        rectTransform.sizeDelta *= scaleMultiplier;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (intensity == 0)
        {
            Graphics.Blit(camTexture, destination);
            return;
        }
        
        mat.SetFloat("_bwBlend", intensity);
        Graphics.Blit(camTexture, destination, mat);     
    }


}
